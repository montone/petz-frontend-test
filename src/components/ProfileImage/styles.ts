import styled from 'styled-components'

import { colors } from '~/styles/theme'

import PropfileImage from '~/assets/profile.jpg'

export const Container = styled.div`
  width: 48px;
  height: 48px;

  border-radius: 50%;

  background: ${`url(${PropfileImage})`} lime;
  background-position: center center;
  background-size: cover;

  border: 2px solid ${colors.primary};
`
