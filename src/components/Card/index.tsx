import React from 'react'

import { Container } from './styles'

const Card: React.FC = ({ children, ...rest }) => {
  return <Container {...rest}>{children}</Container>
}

export default Card
