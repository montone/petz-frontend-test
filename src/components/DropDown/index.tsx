import React from 'react'

import { MdExpandMore } from 'react-icons/md'

import { Wrapper, StyledSelect, Arrow } from './styles'

import { colors } from '~/styles/theme'

interface DropProps {
  dropOptions: Array<string>
  onChange: (e: React.ChangeEvent<HTMLSelectElement>) => void
}

const DropDown: React.FC<DropProps> = ({ dropOptions, ...rest }) => {
  return (
    <Wrapper>
      <StyledSelect {...rest}>
        {dropOptions.map((item, index) => {
          return <option key={String(index)}>{item}</option>
        })}
      </StyledSelect>
      <Arrow>
        <MdExpandMore color={colors.greyHeavy} size="24" />
      </Arrow>
    </Wrapper>
  )
}

export default DropDown
