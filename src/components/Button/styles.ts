import styled, { keyframes } from 'styled-components'

import { ButtonProps } from './index'

export const StyledButton = styled.button<ButtonProps>`
  width: auto;
  height: 32px;
  position: relative;
  padding: 0 32px;
  overflow: hidden;

  display: flex;
  align-items: center;

  font-size: 16px;
  font-weight: 500;
  color: ${p => (p.model === 'outline' ? p.color : '#fff')};

  border: ${p => (p.model === 'outline' ? `1px solid ${p.color}` : '#fff')};
  border-radius: 6px;

  background-color: ${p => (p.model === 'outline' ? 'unset' : p.color)};

  &:active {
    transform: translate(0.5px, 0.5px);
  }

  svg {
    display: inline-block;
  }
`

const animate = keyframes`
  from {
    width: 0;
    height: 0;
    opacity: 0;
  }


  to {
    width: 200px;
    height: 200px;
    opacity: 1;
  }
`

export const FeedBack = styled.div`
  position: absolute;
  width: 0px;
  height: 0px;
  top: 50%;
  left: 50%;
  border-radius: 50%;
  transform: translate(-50%, -50%);
  background: rgba(0, 0, 0, 0.1);
  z-index: 0;

  &&.animate {
    animation: ${animate} 100ms ease-out;
  }
`
