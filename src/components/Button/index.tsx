import React, { useState } from 'react'

import { StyledButton, FeedBack } from './styles'

import { colors } from '~/styles/theme'

export interface ButtonProps {
  color?: string
  type?: 'button' | 'submit'
  model?: 'default' | 'outline'
  onClick?: () => void
}

const Button: React.FC<ButtonProps> = ({
  color = colors.primary,
  model = 'default',
  type = 'button',
  onClick,
  children,
  ...rest
}) => {
  const [feedback, setFeedback] = useState(false)

  function handleClick() {
    setFeedback(false)
    if (onClick) {
      onClick()
    }
  }

  return (
    <StyledButton
      {...rest}
      type={type}
      color={color}
      model={model}
      onClick={() => setFeedback(true)}
      onAnimationEnd={() => handleClick()}
    >
      {children}
      {feedback && (
        <FeedBack
          className={feedback && 'animate'}
          onAnimationEnd={() => handleClick()}
          data-testid="custom-button-feedback"
        />
      )}
    </StyledButton>
  )
}

export default Button
