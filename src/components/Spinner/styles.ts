import styled, { keyframes } from 'styled-components'

const rotation = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(359deg);
  }
`

export const Container = styled.div`
  position: relative;
  width: 24px;
  height: 24px;
  display: inline-block;
  animation: ${rotation} 1s infinite linear;

  svg {
    width: 100%;
    position: absolute;
    top: 50%;
    left: 0;
    transform: translateY(-50%);
  }
`
