import React from 'react'

import { Container } from './styles'
import { colors } from '~/styles/theme'

import { VscLoading } from 'react-icons/vsc'

export interface SpinnerProps {
  color?: string
  size?: string
}

const Spinner: React.FC<SpinnerProps> = ({ color = colors.primary, size }) => {
  return (
    <Container>
      <VscLoading color={color} size={size} />
    </Container>
  )
}

export default Spinner
