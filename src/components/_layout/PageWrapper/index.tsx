import React from 'react'

import Header from '../Header'
import Footer from '../Footer'

import { Wrapper } from './styles'

const PageWrapper: React.FC = ({ children, ...rest }) => {
  return (
    <Wrapper {...rest}>
      <Header />
      {children}
      <Footer />
    </Wrapper>
  )
}

export default PageWrapper
