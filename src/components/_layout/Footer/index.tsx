import React from 'react'

import { Wrapper, Container } from './styles'

const Footer: React.FC = () => {
  return (
    <Wrapper>
      <Container />
    </Wrapper>
  )
}

export default Footer
