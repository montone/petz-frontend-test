import styled from 'styled-components'

import { colors } from '~/styles/theme'

import PageContainer from '../PageContainer'

export const Wrapper = styled.div`
  width: 100%;
  height: 64px;

  display: flex;
  flex-direction: column;
  align-items: center;

  background-color: ${colors.primary};
`

export const Container = styled(PageContainer)``
