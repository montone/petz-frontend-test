import styled from 'styled-components'

import { containerMaxWidth } from '~/styles/theme'

export const Container = styled.div`
  width: 100%;
  max-width: ${containerMaxWidth};
  flex: 1;
`
