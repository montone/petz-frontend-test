import styled from 'styled-components'

import { colors, device } from '~/styles/theme'

import PageContainer from '../PageContainer'

export const Wrapper = styled.div`
  width: 100%;
  height: 120px;

  display: flex;
  flex-direction: column;
  align-items: center;

  background-color: ${colors.secondary};
`

export const Container = styled(PageContainer)`
  padding: 0 16px 0 8px;
  display: flex;
  justify-content: space-between;
  align-items: center;

  @media ${device.tablet} {
    padding: 0 24px 0 8px;
  }

  @media ${device.laptop} {
    padding: unset;
  }
`

export const LogoLink = styled.a`
  width: 90px;
  height: auto;
  display: inline-block;

  img {
    width: 100%;
    height: auto;
  }
`
