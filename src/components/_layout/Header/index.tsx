import React from 'react'

import { Wrapper, Container, LogoLink } from './styles'

import LogoPetz from '~/assets/logo_petz.png'
import ProfileImage from '~/components/ProfileImage'

const Header: React.FC = () => {
  return (
    <Wrapper>
      <Container>
        <LogoLink href="/">
          <img src={LogoPetz} />
        </LogoLink>

        <ProfileImage />
      </Container>
    </Wrapper>
  )
}

export default Header
