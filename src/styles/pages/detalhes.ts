import styled from 'styled-components'

import PageContainer from '~/components/_layout/PageContainer'
import Card from '~/components/Card'

import { colors, device } from '~/styles/theme'

export const Container = styled(PageContainer)`
  flex: 1;

  padding: 0 16px;

  @media ${device.tabletLs} {
    padding: 0;
  }
`

export const TitleArea = styled.div`
  padding: 16px;

  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;

  h1 {
    margin-bottom: 8px;
    text-align: center;
    color: ${colors.greyHeavy};
  }

  @media ${device.tabletLs} {
    padding: 32px 0;

    flex-direction: row;
    justify-content: space-between;
    align-items: center;

    h1 {
      margin-bottom: unset;
    }
  }
`

export const MainContent = styled(Card)`
  margin-bottom: 32px;
`

export const ContentHeader = styled.div`
  position: relative;
  width: 100%;
  height: auto;
  padding: 16px 64px 16px 16px;

  display: flex;
  justify-content: space-between;
  align-items: center;

  border-bottom: 1px solid ${colors.greyLight};
`

export const UserInfo = styled.div`
  display: inline-block;
  margin-right: 8px;

  font-weight: 500;
  font-size: 11.5px;
`

export const UserLabel = styled.span``

export const UserId = styled.span`
  font-size: 17px;
  margin-left: 16px;
  color: ${colors.greyMedium};
`

export const PostDate = styled.div`
  display: inline-block;
  margin: 0 8px;

  flex: 1;

  font-size: 14px;
  font-weight: 400;
  color: ${colors.grey};

  @media ${device.tabletLs} {
    flex: unset;
    margin-top: unset;
  }
`

export const PostDelete = styled.button`
  position: absolute;
  top: 0;
  right: 0;

  width: 64px;
  height: 100%;

  display: flex;
  align-items: center;
  justify-content: center;
  border-left: 1px solid ${colors.greyLight};

  cursor: pointer;

  :active {
    svg {
      transform: translate(1px, 1px);
    }
  }
`

export const PostContent = styled.section`
  padding: 16px;

  h2 {
    margin-bottom: 16px;
  }

  h4 {
    margin-bottom: 16px;
    color: ${colors.primary};
  }
`

export const TextContent = styled.div`
  font-weight: 300;
  font-size: 17px;
  letter-spacing: -0.02em;
  line-height: 24px;
  text-align: left;
  color: ${colors.greyHeavy};
`
