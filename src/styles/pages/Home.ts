import styled from 'styled-components'
import { lighten } from 'polished'

import PageContainer from '~/components/_layout/PageContainer'
import Card from '~/components/Card'

import { colors, device } from '~/styles/theme'

export const Container = styled(PageContainer)`
  flex: 1;

  padding: 0 16px;

  @media ${device.tabletLs} {
    padding: 0;
  }
`

export const TitleArea = styled.div`
  padding: 16px;

  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;

  h1 {
    margin-bottom: 8px;
    text-align: center;
    color: ${colors.greyHeavy};
  }

  @media ${device.tabletLs} {
    padding: 32px 0;

    flex-direction: row;
    justify-content: space-between;
    align-items: center;

    h1 {
      margin-bottom: unset;
    }
  }
`

export const MainContent = styled(Card)`
  margin-bottom: 32px;
`

export const ContentHeader = styled.div`
  width: 100%;
  height: auto;
  padding: 16px;

  display: flex;
  justify-content: space-between;
  align-items: center;

  border-bottom: 1px solid ${colors.greyLight};
`

export const DropWrapper = styled.div`
  width: 200px;
  display: flex;
  align-items: center;
`

export const DropLabel = styled.span`
  display: inline-block;
  margin-right: 8px;

  font-weight: 500;
  font-size: 11.5px;
`

export const PostList = styled.ul`
  list-style-type: none;

  li {
    padding: 16px 8px;

    display: flex;
    flex-direction: column;

    transition: all 0.3s ease;

    @media ${device.tabletLs} {
      flex-direction: row;
      justify-content: space-between;
    }
  }

  li:hover {
    background-color: ${lighten(0.5, colors.primary)};

    .postList__title {
      font-size: 16.5px;
    }
  }

  li + li {
    border-top: 1px solid ${colors.greyLight};
  }

  .postList__title {
    display: inline-block;
    flex: 1;
    font-size: 16px;
    transition: all 0.3s ease;
  }

  .postList__date {
    display: inline-block;

    flex: 1;

    font-size: 14px;
    font-weight: 400;
    color: ${colors.grey};

    @media ${device.tabletLs} {
      flex: unset;
      margin-top: unset;
    }
  }

  button {
    width: 24px;
    height: 24px;
    margin-left: 8px;

    transition: all 0.3s ease;

    :hover {
      transform: scale(1.2, 1.2);
    }
  }
`

export const PostInfo = styled.div`
  display: inline-flex;
  align-items: center;

  padding-top: 8px;
  margin-top: 8px;

  @media ${device.tabletLs} {
    padding-top: unset;
    margin-top: unset;
    margin-left: 8px;
  }
`

export const LoadMore = styled.div`
  margin-bottom: 32px;
  display: flex;
  align-items: center;
  justify-content: center;
`
