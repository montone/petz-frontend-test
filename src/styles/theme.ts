/**
 * COLORS
 */

export const colors = {
  primary: '#00A0E4',
  secondary: ' #FFD400',
  black: '#262626',
  greyHeavy: '#555',
  greyMedium: '#6E6E6E',
  grey: ' #9b9b9b',
  greyLight: '#E0E0E0',
  greyLighter: '#F7F7F7',
  white: '#ffffff',
  statusInfo: '#31A6EF',
  statusSuccess: '#00BA81',
  statusWarning: '#FFCE2C',
  statusDanger: '#FF3761'
}

/**
 * MEDIA QUERIES BREAKPOINTS AND DEVIVCES
 */

// ** Attention for mobile query - max-with **
// *Ls for Landscape ;)

export const windowSize = {
  mobile: '599',
  tablet: '650',
  tabletLs: '770',
  laptop: '1200',
  desktop: '1800'
}

export const device = {
  mobile: `(max-width: ${windowSize.mobile}px)`,
  tablet: `(min-width: ${windowSize.tablet}px)`,
  tabletLs: `(min-width: ${windowSize.tabletLs}px)`,
  laptop: `(min-width: ${windowSize.laptop}px)`,
  desktop: `(min-width: ${windowSize.desktop}px)`,
  desktopL: `(min-width: ${windowSize.desktop}px)`
}

export const containerMaxWidth = '1116px'
