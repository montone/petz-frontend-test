import React, { useEffect, useState } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import api from '~/services/api'

import { MdEdit } from 'react-icons/md'
import { RiDeleteBin2Line } from 'react-icons/ri'

import {
  Container,
  TitleArea,
  MainContent,
  ContentHeader,
  DropWrapper,
  DropLabel,
  PostList,
  PostInfo,
  LoadMore
} from '~/styles/pages/Home'

import { colors } from '~/styles/theme'

import Button from '~/components/Button'
import DropDown from '~/components/DropDown'
import Spinner from '~/components/Spinner'

export interface PostProps {
  id: number
  userId: number
  title: string
  body: string
}

type PostDataProps = {
  data: PostProps
}

const Home: React.FC<PostDataProps> = () => {
  const [loading, setLoading] = useState(false)
  const [postData, setPostData] = useState([])
  const [totalResults, setTotalResults] = useState(null)
  const [page, setPage] = useState(1)
  const [filterUser, setFilterUser] = useState(null)

  const userFilterOptions = ['Todos', '1', '2', '3']

  useEffect(() => {
    async function fetchData() {
      setLoading(true)
      const query = `posts?${
        filterUser ? `userId=${filterUser}&` : ''
      }_page=${page}&_limit=10`

      const response = await api.get(query)

      setPostData([...postData, ...response.data])

      if (!totalResults) {
        setTotalResults(response.headers['x-total-count'])
      }

      setLoading(false)
    }

    fetchData()
  }, [page, filterUser])

  async function handleLoadMore() {
    if (totalResults > postData.length) {
      setPage(page + 1)
    }
  }

  async function handleDelete(id) {
    if (window.confirm(`Você tem certeza que deseja deletar o post (${id})?`)) { /* eslint-disable-line */
      try {
        await api.delete(`/posts/${id}`)

        alert('Post excluido com sucesso!')
      } catch (error) {
        if (error) {
          alert('[Erro] ao deletar o post.')
        }
      }
    }
  }

  function handleUserFilter(e: React.ChangeEvent<HTMLSelectElement>) {
    const selectedIndex = String(e.currentTarget.options.selectedIndex)

    setPostData([])
    setTotalResults(null)
    setPage(1)

    if (selectedIndex === 'Todos') {
      setFilterUser(null)
    } else {
      setFilterUser(selectedIndex)
    }
  }

  return (
    <Container>
      <Head>
        <title>Posts-Listagem</title>
      </Head>

      <TitleArea>
        <h1>Posts {loading && <Spinner />}</h1>

        <Button>Novo Post</Button>
      </TitleArea>

      <MainContent>
        <ContentHeader>
          <DropWrapper>
            <DropLabel>Usuário</DropLabel>
            <DropDown
              onChange={handleUserFilter}
              dropOptions={userFilterOptions}
            />
          </DropWrapper>
        </ContentHeader>

        <PostList>
          {postData.length > 0 &&
            postData.map(post => (
              <li key={String(post.id)}>
                <div className="postList__title">{post.title}</div>
                <PostInfo>
                  <div className="postList__date">28/08/20-09:15</div>
                  <Link href="/detalhes/[id]" as={`detalhes/${post.id}`}>
                    <button>
                      <MdEdit color={colors.primary} size="22" />
                    </button>
                  </Link>
                  <button>
                    <RiDeleteBin2Line
                      onClick={() => handleDelete(post.id)}
                      color={colors.statusDanger}
                      size="22"
                    />
                  </button>
                </PostInfo>
              </li>
            ))}
        </PostList>
      </MainContent>

      {postData.length < totalResults && (
        <LoadMore>
          <Button model="outline" onClick={handleLoadMore}>
            {!loading ? 'Carregar Mais' : <Spinner />}
          </Button>
        </LoadMore>
      )}
    </Container>
  )
}

export default Home
