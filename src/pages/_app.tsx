import React from 'react'
import { AppProps } from 'next/app'

import GlobalStyle from '~/styles/global'

import PageWrapper from '../components/_layout/PageWrapper'

const MyApp: React.FC<AppProps> = ({ Component, pageProps }) => {
  return (
    <PageWrapper>
      <Component {...pageProps} />
      <GlobalStyle />
    </PageWrapper>
  )
}

export default MyApp
