import React, { useEffect, useState } from 'react'
import Head from 'next/head'
import api from '~/services/api'
import { useRouter } from 'next/router'

import { MdDelete, MdChevronLeft } from 'react-icons/md'

import { colors } from '~/styles/theme'

import {
  Container,
  TitleArea,
  MainContent,
  ContentHeader,
  UserInfo,
  UserLabel,
  UserId,
  PostDate,
  PostDelete,
  PostContent,
  TextContent
} from '~/styles/pages/detalhes'

import Button from '~/components/Button'
import Spinner from '~/components/Spinner'

import { PostProps } from '~/pages/index'

const Detalhes: React.FC<PostProps> = () => {
  const router = useRouter()

  const { id } = router.query

  const [loading, setLoading] = useState(false)
  const [postData, setPostData] = useState<Partial<PostProps>>({})

  useEffect(() => {
    async function fetchData() {
      setLoading(true)
      if (id) {
        const response = await api.get(`/posts/${id}`)

        setPostData(response.data)
      }
      setLoading(false)
    }

    fetchData()
  }, [id])

  async function handleDletePost() {
    try {
      await api.delete(`/posts/${id}`)

      alert('Post excluido com sucesso!')

      router.push('/')
    } catch (error) {
      if (error) {
        alert('[Erro] ao deletar o post.')
      }
    }
  }

  return (
    <Container>
      <Head>
        <title>Posts-Detalhes</title>
      </Head>

      <TitleArea>
        <h1>Detalhes {loading && <Spinner />}</h1>

        <Button onClick={() => router.push('/')}>
          <MdChevronLeft size="24" color={colors.secondary} />
          Voltar aos posts
        </Button>
      </TitleArea>

      <MainContent>
        <ContentHeader>
          <UserInfo>
            <UserLabel>Usuário</UserLabel>
            <UserId>{postData.userId}</UserId>
          </UserInfo>

          <PostDate>28/08/20-15:11</PostDate>

          <PostDelete onClick={handleDletePost}>
            <MdDelete color={colors.statusDanger} size="24" />
          </PostDelete>
        </ContentHeader>

        <PostContent>
          <h4>Título</h4>

          <h2>{postData.title}</h2>

          <h4>Conteúdo</h4>

          <TextContent>{postData.body}</TextContent>
        </PostContent>
      </MainContent>
    </Container>
  )
}

export default Detalhes
