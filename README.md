# Petz Front End Test

![Petz Front Test](https://bitbucket.org/montone/petz-frontend-test/raw/c0fb9e3586f964064f0f665c753125265253d65f/_gitFiles/Listagem.jpg)

Aplicação desenvolvida com **ReactJs** utilizando principalmente as seguintes dependências:

- NextJs
- Axios
- Typescript

## Instruções

Eu utilizo principalmente yarn, e as instruções são com ele. Dessa vez eu também deixo usar `npm` no lugar. Mas só desta vez!

- Download ou clone do repositório
- Preciso colocar que deve acessar o diretório `cd petz-frontend-test`?
- `yarn install` para instalar as dependências

### Rodando a aplicação

- Modo desenvolvimento: `yarn dev` - aplicação no modo DEV em `http://localhost:3000/`
- Build: `yarn build` - Build. Só build mesmo O/
- Start: `yarn start`

## Projeto

Desenvolvi o projeto se baseando em um design system próprio que estou desenvolvendo para meus projetos na [Pauleira Guitars](https://www.pauleira.com.br/). Ainda não está pronto mas oferece uma boa base.

Para ver o projeto online: [petz-montone.netlify.app](https://petz-montone.netlify.app/)

Antes de desenvolver as páginas fiz um desenho inicial no **Adobe XD**, mas só fiz a versão desktop para ganhar tempo. Uma base está disponível na pasta **_gitFiles**

Utilizei Next.js pela primeira vez neste projeto e aproveito para praticar algumas outras coisas em testes como o **typescript** por exemplo. Ainda gostaria de ter praticado o **SWR** no lugar do **Axios** para as requisições, porém meu tempo já estava muito apertado.

Fico devendo os **testes** porque o tempo esteve curto pra mim nos últimos dias.

Qualquer dúvida: [montone@gmail.com](mailto:montone@gmail.com)
